<?php

/**
 * @file
 * Rating system database install
 *
 * This file creates the database tables
 */

// Written by Daniel Beeke, daniel@goalgorilla.com

function rating_system_schema() {
  $schema = array();

  $schema['rating_system_rating_groups'] = array(
    'export' => array(
      'key' => 'gsystemname',
      'identifier' => 'preset', // Exports will be defined as $preset
      'default hook' => 'default_rating_system_groups_preset',  // Function hook name.
      'api' => array(
        'owner' => 'rating_system',
        'api' => 'default_rating_system_groups_presets',  // Base name for api include files.
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'gid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'no export' => TRUE, // Do not export database-only keys.
      ),
      'gname' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'gsystemname' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'gdisplay' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
      'gtype' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('gid'),
    'unique keys' => array(
      'name' => array('gsystemname'),
    ),
  );

  $schema['cache_rating_system'] = array(
    'fields' => array(
      'cid' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'data' => array(
        'type' => 'blob',
        'size' => 'big',
        'not null' => FALSE,
      ),
      'expire' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'headers' => array(
        'type' => 'text',
        'not null' => FALSE,
      ),
      'serialized' => array(
        'type' => 'int',
        'size' => 'small',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('cid'),
    'indexes' => array(
      'expire' => array('expire'),
    ),
  );


  return $schema;
}

function rating_system_install() {
  drupal_install_schema('rating_system');
}

function rating_system_uninstall() {
  drupal_uninstall_schema('rating_system');
}