Drupal.behaviors.ratingSystemRuleForm = function (context) {
	
	// Helper on the rule form
	// It hides fields of not selected content types
	$('#edit-contenttype').change(function() {
		$('#edit-fieldname option').hide();
		$.each(Drupal.settings.rating_system_cck.fields[$(this).val()], function(index, value) { 
			$('#edit-fieldname option[value="' + value + '"]').show();
		});
		// Fix default value for changing the select. You can't have a select with a selected item that does not exist.
		//$('#edit-fieldname').val(Drupal.settings.rating_system_cck.fields[$(this).val()][0]);
      // Post the content type and field to a json function which will give info about how to format the value

	});
	$('#edit-fieldname').change(function() {
   		$.post(Drupal.settings.basePath + 'rating-system-cck/give-info/' + $('#edit-contenttype').val() + '/' + $('#edit-fieldname').val(), {}, function(data) {
          $('#edit-cdata-wrapper .description').html(data.info);
      }, "json");
	});
  // On load fix it.
	$('#edit-contenttype').change();  
	$('#edit-fieldname').change();
}