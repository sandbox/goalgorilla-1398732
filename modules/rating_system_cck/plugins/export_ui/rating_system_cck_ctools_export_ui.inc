<?php

/**
 * @file
 * Ctools api file
 *
 * This file creates a configuration screen for the object
 */

/**
* Define this Export UI plugin.
*/
$plugin = array(
  'schema' => 'rating_system_cck',  // As defined in hook_schema().
  'access' => 'administer site configuration',  // Define a permission users must have to access these pages.

  // Define the menu item.
  'menu' => array(
    'menu item' => 'rating_system_cck',
    'menu title' => 'Rating system cck',
    'menu description' => 'Administer Rating cck.',
  ),

  // Define user interface texts.
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('Rating System preset'),
  'title plural proper' => t('Rating System presets'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'rating_system_cck_ctools_export_ui_form',
    // 'submit' and 'validate' are also valid callbacks.
  ),
);

/**
* Define the preset add/edit form.
*/
function rating_system_cck_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];
  $info = _content_type_info();
  
  if ($info['fields']) {
    drupal_add_js(drupal_get_path('module', 'rating_system_cck') . '/js/rating-system-cck-form.js');

    $types = node_get_types();

    foreach ($types as $key_type => $type) {
      foreach ($info['content types'][$key_type]['fields'] as $field) {
        $js_types[$key_type][] = $field['field_name'];
      }
      if ($js_types[$key_type]) {
        $option_types[$key_type] = $type->name;
      }
    }

    drupal_add_js(array('rating_system_cck' => array('fields' => $js_types)), 'setting');

    $fields = $info['fields'];

    foreach ($fields as $field) {
      $option_fields[$field['field_name']] = $field['widget']['label'];
    }

    $groups_data = rating_system_get_rating_groups();

    foreach ($groups_data as $group) {
      $groups[$group->gsystemname] = $group->gname;
    }

    $form['rgroup'] = array(
      '#type' => 'select',
      '#title' => t('Rating group'),
      '#options' => $groups,
      '#default_value' => $preset->rgroup,
    );

    $form['contenttype'] = array(
      '#type' => 'select',
      '#options' => $option_types,
      '#title' => t('Content type'),
      '#default_value' => $preset->contenttype,
      '#required' => TRUE,
    );

    $form['fieldname'] = array(
      '#type' => 'select',
      '#options' => $option_fields,
      '#title' => t('Field'),
      '#default_value' => $preset->fieldname,
      '#required' => TRUE,
    );
    
    $form['tag'] = array(
      '#type' => 'textfield',
      '#title' => t('Tag'),
      '#default_value' => $preset->tag,
      '#required' => TRUE,
    );

    $form['cdata'] = array(
      '#type' => 'textarea',
      '#title' => t('Calculation'),
      '#default_value' => $preset->cdata,
      '#required' => TRUE,
      '#description' => t('Here comes the formatting help for the selected field.'),
    );
  
    if (module_exists('token')) {
      $form['view']['token_help'] = array(
        '#title' => t('Replacement patterns'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
  
      $form['view']['token_help']['help'] = array(
        '#value' => theme('token_help', 'user'),
      );
    }    
  }
  else {
    drupal_set_message(t('Please <a href="/admin/content/types/list">add a cck field</a> first.'));  
  } 
}