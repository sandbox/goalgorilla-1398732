<?php

/**
 * @file
 * Ctools api file
 *
 * This file creates a configuration screen for the object
 */

/**
* Define this Export UI plugin.
*/
$plugin = array(
  'schema' => 'rating_system_final_calculation',  // As defined in hook_schema().
  'access' => 'administer site configuration',  // Define a permission users must have to access these pages.

  // Define the menu item.
  'menu' => array(
    'menu item' => 'rating-system-final-calculation',
    'menu title' => 'Rating: Final calculation',
    'menu description' => 'Administer Rating final calculation.',
  ),

  // Define user interface texts.
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('Rating System preset'),
  'title plural proper' => t('Rating System presets'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'rating_system_final_calculation_ctools_export_ui_form',
    // 'submit' and 'validate' are also valid callbacks.
  ),
);

/**
* Define the preset add/edit form.
*/
function rating_system_final_calculation_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];

  if (isset($preset->rgroup)) {
    $all_hooks = module_invoke_all('define_rating_function', $preset->rgroup, NULL);
    foreach ($all_hooks as $hook_name => $hook) {
      $tokens[] = $hook_name;
    }

    $token_string = '[' . implode('] [', $tokens) . ']';

    $desc = t('You can use the following tokens:') . ' ' . $token_string;
  } 
  else {
    $desc = t('Save the preset and edit it to see the available tokens');
  }

  $groups_data = rating_system_get_rating_groups();

  if ($groups_data) {

    foreach ($groups_data as $group) {
      $groups[$group->gsystemname] = $group->gname;
    }

    $form['rgroup'] = array(
      '#type' => 'select',
      '#title' => t('Rating group'),
      '#options' => $groups,
      '#default_value' => $preset->rgroup,
    );
  }
  else {
    drupal_set_message(t('Please <a href="/admin/build/rating_system_groups/add">add a rating group</a> first.'));  
  }

  $form['cdata'] = array(
    '#type' => 'textarea',
    '#title' => t('Calculation'),
    '#default_value' => $preset->cdata,
    '#required' => TRUE,
    '#description' => $desc,
  );

}