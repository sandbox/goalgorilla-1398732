<?php 

class ctools_export_ui_rating_system_rules extends ctools_export_ui {

	// Borrowed from context module 
  function list_build_row($item, &$form_state, $operations) {
  	drupal_add_css(drupal_get_path('module', 'rating_system_dynamic') . '/css/rating-system-form.css');  	
  	
    $name = $item->name;

    // Add a row for tags.
    $tag = !empty($item->tag) ? $item->tag : t('< Untagged >');
    if (!isset($this->rows[$tag])) {
      $this->rows[$tag]['data'] = array();
      $this->rows[$tag]['data'][] = array('data' => check_plain($tag), 'colspan' => 3, 'class' => 'tag');
      $this->sorts["{$tag}"] = $tag;
    }

    // Build row for each item.
    $this->rows["{$tag}:{$name}"]['data'] = array();
    $this->rows["{$tag}:{$name}"]['class'] = !empty($item->disabled) ? 'ctools-export-ui-disabled' : 'ctools-export-ui-enabled';
    $this->rows["{$tag}:{$name}"]['data'][] = array(
      'data' => check_plain($name),
      'class' => 'ctools-export-ui-name'
    );
    $this->rows["{$tag}:{$name}"]['data'][] = array(
      'data' => check_plain($item->type),
      'class' => 'ctools-export-ui-storage'
    );
    $this->rows["{$tag}:{$name}"]['data'][] = array(
      'data' => theme('links', $operations, array('class' => 'links inline')),
      'class' => 'ctools-export-ui-operations'
    );

    // Sort by tag, name.
    $this->sorts["{$tag}:{$name}"] = $tag . $name;
  }

}