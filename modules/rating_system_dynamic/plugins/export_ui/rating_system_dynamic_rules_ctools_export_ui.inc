<?php

/**
 * @file
 * Ctools api file
 *
 * This file creates a configuration screen for the object
 */

/**
* Define this Export UI plugin.
*/
$plugin = array(
  'schema' => 'rating_system_rules',  // As defined in hook_schema().
  'access' => 'administer site configuration',  // Define a permission users must have to access these pages.

  // Define the menu item.
  'menu' => array(
    'menu item' => 'rating-system-rules',
    'menu title' => 'Rating: Rules',
    'menu description' => 'Administer Rating System presets.',
  ),
  
  // Adding our own class for advanced listing
	//*  
  'handler' => array(
     'class' => 'ctools_export_ui_rating_system_rules',
     'parent' => 'ctools_export_ui',
   ),  
	//*/
  // Define user interface texts.
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('Rating System preset'),
  'title plural proper' => t('Rating System presets'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'rating_system_dynamic_rules_ctools_export_ui_form',
    'submit' => 'rating_system_dynamic_rules_ctools_export_ui_form_submit',
  ),
);

/**
* Define the preset add/edit form.
*/
function rating_system_dynamic_rules_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];

  drupal_add_js(drupal_get_path('module', 'rating_system') . '/js/rating-system-form.js');
  
  if (isset($preset->pidsystemname)) {
    $form['pid'] = array(
      '#type' => 'value',
      '#value' => db_result(db_query("select pid FROM {rating_system_points} WHERE pidsystemname = '%s'", array($preset->pidsystemname))),
    );
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Human name'),
    '#default_value' => $preset->name,
    '#required' => TRUE,
  );

  $groups_data = rating_system_get_rating_groups();

  foreach ($groups_data as $group) {
    $groups[$group->gsystemname] = $group->gname;
  }

  $form['pidsystemname'] = array(
    '#type' => 'value',
    '#value' => $preset->pidsystemname,
  );

  $form['rtype'] = array(
    '#type' => 'select',
    '#title' => t('Rating group'),
    '#options' => $groups,
    '#default_value' => $preset->rtype,
  );

  $calculation = db_result(db_query("select calculation FROM {rating_system_points} WHERE pidsystemname = '%s'", array($preset->pidsystemname)));

  $form['calculation'] = array(
    '#type' => 'textfield',
    '#title' => t('Calculation'),
    '#default_value' => $calculation,
    '#required' => TRUE,
  );

  $form['tag'] = array(
    '#type' => 'textfield',
    '#title' => t('Tag'),
    '#default_value' => $preset->tag,
    '#required' => TRUE,
  );

  if (module_exists('token')) {
    $form['view']['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['view']['token_help']['help'] = array(
      '#value' => theme('token_help', 'user'),
    );
  }

}

function rating_system_dynamic_rules_ctools_export_ui_form_submit(&$form, &$form_state) {
  $point = array(
    'calculation' => $form_state['values']['calculation'],
    'pidsystemname' => $form_state['values']['systemname'] . '_points',
  );

  if ($form_state['values']['pid']) {
    $result = drupal_write_record('rating_system_points', $point, array('pidsystemname'));
  } 
  else {
    $result = drupal_write_record('rating_system_points', $point);
  }

  $form_state['values']['pidsystemname'] = $point['pidsystemname'];
}