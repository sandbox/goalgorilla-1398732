<?php

/**
 * Implementation of hook_default_rating_system_points_preset().
 */
function rating_system_test_feature_default_rating_system_points_preset() {
  $export = array();

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->pid = '5';
  $preset->pidsystemname = 'user_views_a_page_points';
  $preset->calculation = '+1';
  $export['user_views_a_page_points'] = $preset;

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->pid = '4';
  $preset->pidsystemname = 'users_edit_s_a_page_points';
  $preset->calculation = '+1';
  $export['users_edit_s_a_page_points'] = $preset;

  return $export;
}
