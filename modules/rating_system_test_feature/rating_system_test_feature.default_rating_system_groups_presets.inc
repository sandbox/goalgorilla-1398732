<?php

/**
 * Implementation of hook_default_rating_system_groups_preset().
 */
function rating_system_test_feature_default_rating_system_groups_preset() {
  $export = array();

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->gname = 'Page points';
  $preset->gsystemname = 'page_points';
  $preset->gdisplay = 1;
  $preset->gtype = 'page';
  $export['page_points'] = $preset;

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->gname = 'Profile Points';
  $preset->gsystemname = 'profile_points';
  $preset->gdisplay = 1;
  $preset->gtype = 'profile';
  $export['profile_points'] = $preset;

  return $export;
}
