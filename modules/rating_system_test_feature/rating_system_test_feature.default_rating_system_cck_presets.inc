<?php

/**
 * Implementation of hook_default_rating_system_cck_preset().
 */
function rating_system_test_feature_default_rating_system_cck_preset() {
  $export = array();

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->cid = '8';
  $preset->systemname = 'profile-field_about_me';
  $preset->contenttype = 'profile';
  $preset->fieldname = 'field_about_me';
  $preset->tag = 'personal_information';
  $preset->rgroup = 'profile_points';
  $preset->cdata = '+1';
  $export['profile-field_about_me'] = $preset;

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->cid = '9';
  $preset->systemname = 'profile-field_i_am_a';
  $preset->contenttype = 'profile';
  $preset->fieldname = 'field_i_am_a';
  $preset->tag = 'personal_information';
  $preset->rgroup = 'profile_points';
  $preset->cdata = 'noob|+20
ninja|+50';
  $export['profile-field_i_am_a'] = $preset;

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->cid = '6';
  $preset->systemname = 'profile-field_name';
  $preset->contenttype = 'profile';
  $preset->fieldname = 'field_name';
  $preset->tag = 'personal_information';
  $preset->rgroup = 'profile_points';
  $preset->cdata = '+1';
  $export['profile-field_name'] = $preset;

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->cid = '7';
  $preset->systemname = 'profile-field_surname';
  $preset->contenttype = 'profile';
  $preset->fieldname = 'field_surname';
  $preset->tag = 'personal_information';
  $preset->rgroup = 'profile_points';
  $preset->cdata = '+1';
  $export['profile-field_surname'] = $preset;

  return $export;
}
