<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function rating_system_test_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "rating_system" && $api == "default_rating_system_groups_presets") {
    return array("version" => 1);
  }
  elseif ($module == "rating_system" && $api == "default_rating_system_points_presets") {
    return array("version" => 1);
  }
  elseif ($module == "rating_system_cck" && $api == "default_rating_system_cck_presets") {
    return array("version" => 1);
  }
  elseif ($module == "rating_system_final_calculation" && $api == "default_rating_system_final_calculation_presets") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function rating_system_test_feature_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Page'),
      'module' => 'features',
      'description' => t('A <em>page</em>, similar in form to a <em>story</em>, is a simple method for creating and displaying information that rarely changes, such as an "About us" section of a website. By default, a <em>page</em> entry does not allow visitor comments and is not featured on the site\'s initial home page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'profile' => array(
      'name' => t('Profile'),
      'module' => 'features',
      'description' => t('A user profile built as content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_rules_defaults().
 */
function rating_system_test_feature_rules_defaults() {
  return array(
    'rules' => array(
      'rules_user_edit_s_a_page' => array(
        '#type' => 'rule',
        '#set' => 'event_node_update',
        '#label' => 'User edits a page',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'user_actions',
          'rating_system_test_feature' => 'rating_system_test_feature',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Updated content is Page',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'module' => 'Node',
            ),
            '#name' => 'rules_condition_content_is_type',
            '#settings' => array(
              'type' => array(
                'page' => 'page',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#weight' => 0,
            '#type' => 'action',
            '#settings' => array(
              'rating_system_dynamic_action' => 'users_edit_s_a_page',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'rating_system_dynamic_action' => array(
                    '0' => ':global',
                  ),
                ),
              ),
            ),
            '#name' => 'rating_system_dynamic_add_dynamic_action_rules',
            '#info' => array(
              'label' => 'Add dynamic action',
              'arguments' => array(
                'rating_system_dynamic_action' => array(
                  'type' => 'string',
                  'label' => 'Action',
                ),
              ),
              'module' => 'Rating system dynamic',
            ),
          ),
        ),
        '#version' => 6003,
      ),
      'rules_user_views_a_page' => array(
        '#type' => 'rule',
        '#set' => 'event_node_view',
        '#label' => 'User views a page',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'user_actions',
          'rating_system_test_feature' => 'rating_system_test_feature',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Viewed content is Page',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'module' => 'Node',
            ),
            '#name' => 'rules_condition_content_is_type',
            '#settings' => array(
              'type' => array(
                'page' => 'page',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#weight' => 0,
            '#type' => 'action',
            '#settings' => array(
              'rating_system_dynamic_action' => 'user_views_a_page',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'rating_system_dynamic_action' => array(
                    '0' => ':global',
                  ),
                ),
              ),
            ),
            '#name' => 'rating_system_dynamic_add_dynamic_action_rules',
            '#info' => array(
              'label' => 'Add dynamic action',
              'arguments' => array(
                'rating_system_dynamic_action' => array(
                  'type' => 'string',
                  'label' => 'Action',
                ),
              ),
              'module' => 'Rating system dynamic',
            ),
          ),
        ),
        '#version' => 6003,
      ),
    ),
  );
}
