Drupal.behaviors.ratingSystemRuleForm = function (context) {
	
	// Helper on the rule form
	// It creates a machine name for the action	
	
	$('#' + $('label:contains("Human name")').attr('for')).bind('keyup change', function() {
		var machine = $(this).val().toLowerCase().replace(/[^a-z0-9]+/g, '_').replace(/_+/g, '_');
		$('#' + $('label:contains("Name")').attr('for')).val(machine);
	});
	
	$('#' + $('label:contains("Name")').attr('for')).parent().hide();
}