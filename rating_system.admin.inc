<?php

/**
 * @file
 * Rating system settings file
 *
 * This file creates a settings form for the rating system module
 */

/**
 * Settings form
 */
function rating_system_settings_form($form_state) {
  $form = array();
  $form['cache'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cache'),
  );

  $form['cache']['rating_system_cache_all'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('rating_system_cache_all', FALSE),
    '#title' => t('Flush the cache when flushing the system all cache'),
  );

  $form['cache']['flush'] = array(
    '#type' => 'submit',
    '#value' => t('Flush cache'),
    '#submit' => array('rating_system_settings_flush_redirect'),
  );
  return system_settings_form($form);
}

/**
 * Settings form redirect
 */
function rating_system_settings_flush_redirect() {
  drupal_goto('admin/settings/rating-system/flush-confirmation');
}

/**
 * Confirm form
 */
function rating_system_flush_form($form_state) {
  $form = array();

  $form['flush'] = array(
    '#type' => 'submit',
    '#value' => t('Yes, Flush the rating system cache'),
    '#submit' => array('rating_system_settings_flush_submit'),
  );
  return $form;
}

/**
 * Clear the rating system cache
 */
function rating_system_settings_flush_submit($form, $form_state) {
  cache_clear_all('*', 'cache_rating_system', TRUE);
  drupal_set_message('The rating system cache has been flushed');
  drupal_goto('admin/settings/rating-system');
}
