<?php

/**
 * @file
 * Ctools api file
 *
 * This file creates a configuration screen for the object
 */

/**
* Define this Export UI plugin.
*/
$plugin = array(
  'schema' => 'rating_system_rating_groups',  // As defined in hook_schema().
  'access' => 'administer site configuration',  // Define a permission users must have to access these pages.

  // Define the menu item.
  'menu' => array(
    'menu item' => 'rating-system-groups',
    'menu title' => 'Rating: Groups',
    'menu description' => 'Administer Rating System presets.',
  ),

  // Define user interface texts.
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('Rating System preset'),
  'title plural proper' => t('Rating System presets'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'rating_system_groups_ctools_export_ui_form',
    // 'submit' and 'validate' are also valid callbacks.
  ),
);

/**
* Define the preset add/edit form.
*/
function rating_system_groups_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];

  drupal_add_js(drupal_get_path('module', 'rating_system') . '/js/rating-system-form.js');

  $form['gname'] = array(
    '#type' => 'textfield',
    '#title' => t('Human name'),
    '#default_value' => $preset->gname,
    '#required' => TRUE,
  );

  foreach ($types = node_get_types() as $type) {
    $options[$type->type] = $type->name;
  }

  $form['gdisplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display this rating under the nodetype inside the links array'),
    '#description' => t("Don't worry, if you don't select this. The variables['rating_<em>{systemname}</em>'] will always be available."),
    '#default_value' => $preset->gdisplay,
  );

  $form['gtype'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => $options,
    '#default_value' => $preset->gtype,
    '#required' => TRUE,
  );
}